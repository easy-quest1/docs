 
<h1 id="cover-heading">my-syte</h1>

%%[![GitHub tags](https://img.shields.io/github/tag/easy-quest/web.svg)](https://GitHub.com/easy-quest/web-docs/tags/)%% 

>  Начальный шаблон для сайта документов на основе Markdown <!-- TODO: Заменить описанием -->


- :hourglass_flowing_sand: Быстрая настройка элегантного, отзывчивого сайта
- :open_file_folder: Использование документов с разметкой в качестве содержимого
- :sparkles: Нет шага компиляции и нет синтаксиса шаблонов для изучения
- :nut_and_bolt: Построено на [DocsifyJS](https://docsify.js.org/)
- :pushpin: Библиотека загружается в браузере - нет локальных зависимостей
- :cloud: Обслуживание локально и на страницах GitHub или Netlify


![color](#b3d9f8)
